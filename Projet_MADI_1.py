# -*- coding: utf-8 -*-
"""
Created on Tue Oct 21 17:12:10 2014

@author: rAyyy
"""

# script pion.py
from Tkinter import *
import numpy
from gurobipy import *
import time #pour l'estimation du temps de calcul


def initialize():
    global PosX,PosY,cost,test_dji,p,gamma,recompense_factor,target_recompense
    #on met à jour les différentes variable misent dans les entrées
    p=entry_p.get()
    gamma=entry_gamma.get()
    recompense_factor=entry_recompense_factor.get()
    target_recompense=entry_target_recompense.get()
    print p,gamma,recompense_factor,target_recompense
    Canevas.focus_set()
    Canevas.bind('<Key>',Clavier)
# position initiale du robot
    PosX = 20+10*zoom
    PosY = 20+10*zoom
    for k in range(5):
        cost[k]=0
    
    if test_dji==True:
        for i in Dji_line:         
                Canevas.delete(i)
        test_dji=False
# cout et affichage
    Canevas.coords(Pion,PosX -9*zoom, PosY -9*zoom, PosX +9*zoom, PosY +9*zoom)
    w.config(text='Cost = '+ str(cost[0]))

def colordraw(g,nblignes,nbcolonnes):
    pblanc=0.15#0.15
    pverte=0.35
    pbleue=0.25
    prouge=0.15
    pnoire=0.10
    for i in range(nblignes):
        for j in range(nbcolonnes):
            z=np.random.uniform(0,1)
            if z < pblanc:
                c=0
            else:
                if z < pblanc+ pverte:
                    c=1
                else:
                    if z < pblanc+ pverte + pbleue:
                        c=2
                    else:
                        if z< pblanc+ pverte + pbleue +prouge:
                            c=3
                        else:
                            c=4   
            if c>0:
                g[i,j]=c
    g[0,0]=np.random.random_integers(3)
    g[0,1]=np.random.random_integers(3)
    g[2,0]=np.random.random_integers(3)     
    g[nblignes-1,nbcolonnes-1]=np.random.random_integers(3)
    g[nblignes-2,nbcolonnes-1]=np.random.random_integers(3)
    g[nblignes-1,nbcolonnes-2]=np.random.random_integers(3)
    for i in range(nblignes):
        for j in range(nbcolonnes):          
            y =zoom*20*i+20
            x =zoom*20*j+20
            if g[i,j]>0:            
                Canevas.create_oval(x+zoom*(10-3),y+zoom*(10-3),x+zoom*(10+3),y+zoom*(10+3),width=3.0,outline=color[g[i,j]],fill=color[g[i,j]])
                #Canevas.create_line(x, y+ 10*zoom, x+zoom*20, y+zoom*10,  arrow = "last")
            else:
                #Canevas.create_rectangle(x, y+zoom*10, x+zoom*20, y+zoom*10, fill=myblack)
                Canevas.create_rectangle(x, y, x+zoom*20, y+zoom*20, fill=myblack)

def Clavier(event):
    global PosX,PosY,cost,g
    touche = event.keysym
    cj=(PosX-30)/(20*zoom)
    li=(PosY-30)/(20*zoom)
    #print(li,cj)
    # deplacement vers le haut
    v="v"
    b="b"
    #on cherche en fonction de la touche le voisin vers lequel on se dirige (si il existe)
    if touche == 'a' and li>0 and g[li-1,cj]>0:
            v,b = getneibhor(li,cj,0)
        # deplacement vers le bas
    if touche == 'q' and li<nblignes-1 and g[li+1,cj]>0:
            v,b = getneibhor(li,cj,2)
        # deplacement vers la droite
    if touche == 'm' and cj< nbcolonnes-1 and g[li,cj+1]>0:
            v,b = getneibhor(li,cj,1)
        # deplacement vers la gauche
    if touche == 'l' and cj >0 and g[li,cj-1]>0:
            v,b = getneibhor(li,cj,3)
    if touche == 'space':        
        pol=pol_final_tampon[li,cj]
        print "pol",pol
        v,b = getneibhor(li,cj,pol)
    #si on a un voisin on se dirige vers lui
    if v!="v" :
        
        
        rndm=np.random.uniform(0,1)
        print cj,li
        
        
        print "voisin",v,b
        if b==0:
            print "?"
        elif b==1:
            PosY=v[0][0]*(20*zoom)+30
            PosX=v[0][1]*(20*zoom)+30
        elif b==2:
            if rndm < ((1+p)/2):
                PosY=v[0][0]*(20*zoom)+30
                PosX=v[0][1]*(20*zoom)+30
            else:
                PosY=v[1][0]*(20*zoom)+30
                PosX=v[1][1]*(20*zoom)+30
        elif b==3:
            if rndm < p:
                PosY=v[0][0]*(20*zoom)+30
                PosX=v[0][1]*(20*zoom)+30
            elif rndm< p + ((1-p)/2):
                PosY=v[1][0]*(20*zoom)+30
                PosX=v[1][1]*(20*zoom)+30
            else:
                PosY=v[2][0]*(20*zoom)+30
                PosX=v[2][1]*(20*zoom)+30
        cj=(PosX-30)/(20*zoom)
        li=(PosY-30)/(20*zoom)
        cost[g[li,cj]]+=1
        

    # on dessine le pion a sa nouvelle position
    Canevas.coords(Pion,PosX -9*zoom, PosY -9*zoom, PosX +9*zoom, PosY +9*zoom)
    cost[0]=0    
    for k in range(4):
        cost[0]+=cost[k+1]*weight[k+1]
    w.config(text='Cost = '+ str(cost[0]))
#############################################################################################
def Dijkstra():
    #calcul du chemin optimal
    start = time.time()
    #tableau des distances de chaque noeud
    dist = np.zeros((nblignes,nbcolonnes), dtype=numpy.float)
    #tableau du meilleur voisin pour chaque noeud
    previous = np.zeros((nblignes,nbcolonnes,2), dtype=numpy.int)
    #on ajoute dans Q tt les noeuds possible
    Q= []
    dist[0,0]=0
    for i in range (nblignes):
        for j in range (nbcolonnes):
            inc=0
            for k in range (4):
                v,b=getneibhor(i,j,k)
                if b==0:
                    inc+=1
            if inc!=4:
                if j+i != 0:
                    dist[i,j]=1000**recompense_factor
                    previous[i,j]=(0,0)
                if g[i,j]>0:
                    Q.append((i,j))
    
    while len(Q)!=0:
        a=50000
        u=()
        #on choisit le noeud le plus proche et qui n'a pas encore était prit en compte
        for item in Q:
            if a > dist[item]:
                a= dist[item]
                u=item

        Q.remove(u)
        
        v= neibhor(u)
        #on calcul la distance pour ce point et son meilleur voisin
        for indx in v:
            if Q.count(indx)==1:
                #print indx
                alt= dist[u]+g[indx]**recompense_factor
                if alt < dist[indx]:
                    dist[indx]=alt
                    previous[indx]=u

    print "coutoptimal pr la destination choisi",dist[nblignes-1,nbcolonnes-1]
    S=[]
    u=(nblignes-1,nbcolonnes-1)
    S.append(u)
    #on remonte les meilleurs voisin de tout les noeuds pour avoir le meilleur chemin
    t1=previous[u][0]
    t2=previous[u][1]
    while t1 +t2 !=0:
        S.append((t1,t2))
        u= previous[t1,t2]
        t1=u[0]
        t2=u[1]
    end=time.time()
    print "ellapsed time:", end - start
    return S


def neibhor(v):
    #retourne la liste de tout les voisin possible de v
    neigbhor_list= []
    neigbhor_list.append((v[0]-1,v[1]))
    neigbhor_list.append((v[0],v[1]-1))
    neigbhor_list.append((v[0],v[1]+1))
    neigbhor_list.append((v[0]+1,v[1]))
    
    if  v[0]== 0:
        neigbhor_list.remove((v[0]-1,v[1]))
    if v[1] == 0:
        neigbhor_list.remove((v[0],v[1]-1))
    if v[0] == nblignes-1:
        neigbhor_list.remove((v[0]+1,v[1]))
    if v[1] == nbcolonnes-1:
        neigbhor_list.remove((v[0],v[1]+1))  
        
    return neigbhor_list   
########################################################################################
def PDM_Iter_Val():
    start = time.time()
    print""
    print "PDM"
    global pol_final
    #contien la politique calculé au temp t
    pol_final= np.zeros((nblignes,nbcolonnes), dtype=numpy.int)#prend en valeur 0,1,2,3, NESW
    global pol_final_tampon  #contien la politique calculé au temp t-1
    pol_final_tampon= np.zeros((nblignes,nbcolonnes), dtype=numpy.int)#prend en valeur 0,1,2,3, NESW
    
    politiqueAll = np.zeros((nblignes,nbcolonnes,4), dtype=numpy.float) 
    politiqueAll_tampon = np.zeros((nblignes,nbcolonnes,4), dtype=numpy.float)
    
    erreur_tab=np.zeros((nblignes,nbcolonnes), dtype=numpy.float)
    
    #defini la fonction recompense
    recompense = np.zeros((nblignes,nbcolonnes), dtype=numpy.int)
    for i in range(nblignes):
       for j in range(nbcolonnes): 
           if i+j == nblignes + nbcolonnes -2:
               recompense[i,j]=target_recompense**recompense_factor
           else:
               recompense[i,j]=-(g[i,j]**recompense_factor)
    
    epsilon=0.1
    erreur=5*epsilon
    m=0
    #temps que l'erreur est trop grande on itére
    while erreur > epsilon:
       m=m+1
       for i in range(nblignes):
           for j in range(nbcolonnes): 
               max_info=[]#prend la politique maximal suivant l'aaction
               for k in range(4):
                   v,b=getneibhor(i,j,k)


                   if b==1:
                       politiqueAll[i,j,k]=recompense[v[0]]+gamma*(1*politiqueAll_tampon[v[0][0],v[0][1],pol_final[v[0][0],v[0][1]]])
                   if b==2:
                       politiqueAll[i,j,k]=recompense[v[0]] + gamma*( ((1+p)/2.0)*politiqueAll_tampon[v[0][0],v[0][1],pol_final[v[0][0],v[0][1]]] + ((1-p)/2.0)*politiqueAll_tampon[v[1][0],v[1][1],pol_final[v[1][0],v[1][1]]])
                   if b==3:
                       politiqueAll[i,j,k]=recompense[v[0]] + gamma*(p*politiqueAll_tampon[v[0][0],v[0][1],pol_final[v[0][0],v[0][1]]] +  ((1-p)/2.0)*politiqueAll_tampon[v[1][0],v[1][1],pol_final[v[1][0],v[1][1]]] +((1-p)/2.0)*politiqueAll_tampon[v[2][0],v[2][1],pol_final[v[2][0],v[2][1]]]  )                      
                   if max_info==[]:
                       if b>0:
                           max_info=[politiqueAll[i,j,k],k]
                   if b>0:
                       if politiqueAll[i,j,k]>max_info[0]:
                           max_info[0]=politiqueAll[i,j,k]
                           max_info[1]=k
               if max_info!=[]:
                   pol_final_tampon[i,j]=max_info[1]
               erreur_tab[i,j]=abs(politiqueAll[i,j,pol_final_tampon[i,j]] - politiqueAll_tampon[i,j,pol_final[i,j]])
               erreur=erreur_tab.max()#l'erreur maximal à minimisé pour arreter les calculs
               
       pol_final=pol_final_tampon
       politiqueAll_tampon=politiqueAll
       politiqueAll=np.zeros((nblignes,nbcolonnes,4), dtype=numpy.float)
       

    print "M" , m #nombre d'itération
    print "er",erreur
    end=time.time()
    print "ellapsed time:", end - start
    print "pol",pol_final_tampon
    affichage_politique()#dessine la politique
    
    return end - start,m
    
def getneibhor(i,j,k):#renvoi les voisins possibles sur la direction k
    neib=[]
    tampon=[]
    main=()
    if k ==0:
        main =(i-1,j)
        neib.append((i-1,j))
        neib.append((i-1,j-1))
        neib.append((i-1,j+1))
        tampon.append((i-1,j))
        tampon.append((i-1,j-1))
        tampon.append((i-1,j+1))
    if k ==1:
        main=(i,j+1)
        neib.append((i,j+1))
        neib.append((i+1,j+1))
        neib.append((i-1,j+1))
        tampon.append((i,j+1))
        tampon.append((i+1,j+1))
        tampon.append((i-1,j+1))
    if k ==2:
        main=(i+1,j)
        neib.append((i+1,j))
        neib.append((i+1,j+1))
        neib.append((i+1,j-1))
        tampon.append((i+1,j))
        tampon.append((i+1,j+1))
        tampon.append((i+1,j-1))
    if k ==3:
        main=(i,j-1)
        neib.append((i,j-1))
        neib.append((i-1,j-1))
        neib.append((i+1,j-1))
        tampon.append((i,j-1))
        tampon.append((i-1,j-1))
        tampon.append((i+1,j-1))
    for v in tampon:
        if  v[0]< 0:
            neib.remove(v)
        elif v[1] < 0:
            neib.remove(v)
        elif v[0] > nblignes-1:
            neib.remove(v)
        elif v[1] > nbcolonnes-1:
            neib.remove(v)    
        elif g[v]==0:
            neib.remove(v)  
    if neib.count(main)==0:
        neib=[]

    return neib , len(neib)
    
def PDM_Iter_Pol():
    start = time.time()
    print""
    print "PDM iteration politique"
    global pol_final
    pol_final= np.zeros((nblignes,nbcolonnes), dtype=numpy.int)#0,1,2,3, Nord Est Sud Ouest
    global pol_final_tampon
    pol_final_tampon= np.zeros((nblignes,nbcolonnes), dtype=numpy.int)
    
    politiqueAll = np.zeros((nblignes,nbcolonnes,4), dtype=numpy.float) 
    politiqueAll_tampon = np.zeros((nblignes,nbcolonnes,4), dtype=numpy.float)
    
    erreur_tab=np.zeros((nblignes,nbcolonnes), dtype=numpy.float)
    
    recompense = np.zeros((nblignes,nbcolonnes), dtype=numpy.int)
    for i in range(nblignes):
       for j in range(nbcolonnes): 
           if i+j == nblignes + nbcolonnes -2:
               recompense[i,j]=target_recompense
           else:
               recompense[i,j]=-(g[i,j]**recompense_factor)
    
    m=0
    test_fin=True
    while test_fin:#temps qu'il y a des changement on calcul
       m=m+1
       test_fin=False
       for i in range(nblignes):
           for j in range(nbcolonnes): 
               max_info=[]
               for k in range(4):
                   v,b=getneibhor(i,j,k)
                   if b==1:
                       politiqueAll[i,j,k]=recompense[v[0]]+gamma*(1*politiqueAll_tampon[v[0][0],v[0][1],pol_final[v[0][0],v[0][1]]])
                   if b==2:
                       politiqueAll[i,j,k]=recompense[v[0]] + gamma*( ((1+p)/2.0)*politiqueAll_tampon[v[0][0],v[0][1],pol_final[v[0][0],v[0][1]]] + ((1-p)/2.0)*politiqueAll_tampon[v[1][0],v[1][1],pol_final[v[1][0],v[1][1]]])
                   if b==3:
                       politiqueAll[i,j,k]=recompense[v[0]] + gamma*(p*politiqueAll_tampon[v[0][0],v[0][1],pol_final[v[0][0],v[0][1]]] +  ((1-p)/2.0)*politiqueAll_tampon[v[1][0],v[1][1],pol_final[v[1][0],v[1][1]]] +((1-p)/2.0)*politiqueAll_tampon[v[2][0],v[2][1],pol_final[v[2][0],v[2][1]]]  )                      
                   if max_info==[]:
                       if b>0:
                          max_info=[politiqueAll[i,j,k],k]
                   if b>0:
                       if politiqueAll[i,j,k]>max_info[0]:
                           max_info[0]=politiqueAll[i,j,k]
                           max_info[1]=k
               if max_info!=[]:
                   if pol_final_tampon[i,j]!=max_info[1]:
                       pol_final_tampon[i,j]=max_info[1]
                       test_fin = True
       pol_final=pol_final_tampon
       politiqueAll_tampon=politiqueAll
       politiqueAll=np.zeros((nblignes,nbcolonnes,4), dtype=numpy.float)

    print "M" , m
    end=time.time()
    print "ellapsed time:", end - start
    print "pol",pol_final_tampon
    affichage_politique()
    return end - start,m
    
def PDM_PL():
    start = time.time()
    print""
    print "PDM res par PL"
    global pol_final_tampon
    pol_final_tampon= np.zeros((nblignes,nbcolonnes), dtype=numpy.int)
    
    recompense = np.zeros((nblignes,nbcolonnes), dtype=numpy.float)
    for i in range(nblignes):
       for j in range(nbcolonnes): 
           if i+j == nblignes + nbcolonnes -2:
               recompense[i,j]=target_recompense**recompense_factor
           else:
               recompense[i,j]=(5-g[i,j])**recompense_factor
               
    m = Model("mdp")    

    #ajout des variables pour chaque cases
    v=np.zeros((nblignes,nbcolonnes), dtype=gurobipy.Var)
    for i in range(nblignes):
        for j in range(nbcolonnes): 
            v[i,j]=m.addVar(vtype=GRB.CONTINUOUS, lb=0, name="v%d" % (i*10+j))
            
    m.update()


    obj = LinExpr();
    obj =0
    obj+=v[0,0]#on part de la case (0,0)
    
    
    m.setObjective(obj,GRB.MINIMIZE)
    
    #on ajoute les contraintes
    for i in range(nblignes):
        for j in range(nbcolonnes): 
            for k in range(4):
                   a,b=getneibhor(i,j,k)
                   if b==1:
                       m.addConstr(v[i,j] - gamma*(v[a[0]])  >= recompense[a[0]])
                   if b==2:
                       m.addConstr(v[i,j] - gamma*(((1+p)/2)*v[a[0]]      +   ((1-p)/2)*v[a[1]]    )>= recompense[a[0]])
                   if b==3:
                       m.addConstr(v[i,j] - gamma*(p*v[a[0]]      +   ((1-p)/2)*v[a[1]] + ((1-p)/2)*v[a[2]]   )>= recompense[a[0]])

    m.optimize()
    
    #on calcul pour faire le choix de la politique
    for i in range(nblignes):
        for j in range(nbcolonnes): 
            max_info=[]
            politique=0
            for k in range(4):
                a,b=getneibhor(i,j,k)
                
                if b==1:
                       politique=recompense[a[0]]+gamma*(v[a[0]].x)
                if b==2:
                       politique=recompense[a[0]] + gamma*(((1+p)/2)*v[a[0]].x      +   ((1-p)/2)*v[a[1]].x    )
                if b==3:
                       politique=recompense[a[0]] + gamma*(p*v[a[0]].x      +   ((1-p)/2)*v[a[1]].x + ((1-p)/2)*v[a[2]].x   )          
                if max_info==[]:
                    if b>0:
                        max_info=[politique,k]
                if b>0:
                    if politique>max_info[0]:
                        max_info[0]=politique
                        max_info[1]=k
            if max_info!=[]:
                pol_final_tampon[i,j]=max_info[1]
    end=time.time()
    print "ellapsed time:", end - start
    print "pol", pol_final_tampon
    affichage_politique()
    print "fin PL"
    return end - start
                

########################################################################################
def djikarta_operation():#permet de géré les dessins des flèches
    global Dji_line , test_dji
    if test_dji==False:
        A=Dijkstra()
        Dji_line=[]
        
        for i in range(nblignes):
           for j in range(nbcolonnes):          
                y =zoom*20*i+20
                x =zoom*20*j+20
                if A.count((i,j))==1:    
                    Dji_line.append(Canevas.create_line(x, y, x+zoom*20, y+zoom*20,  arrow = "last"))
        test_dji=True
        
def affichage_politique():#dessine une politique
        global Pol_line
        for i in Pol_line:         
                Canevas.delete(i)
        Pol_line=[]
        for i in range(nblignes):
            for j in range(nbcolonnes): 
                y =zoom*20*i+20
                x =zoom*20*j+20
                if g[i,j]!=0:
                    if pol_final_tampon[i,j]==0:
                        Pol_line.append(Canevas.create_line(x+zoom*10, y+20*zoom, x+zoom*10, y,  arrow = "last"))
                    if pol_final_tampon[i,j]==1:
                        Pol_line.append(Canevas.create_line(x, y+10*zoom, x+zoom*20, y+zoom*10,  arrow = "last"))
                    if pol_final_tampon[i,j]==2:
                        Pol_line.append(Canevas.create_line(x+zoom*10, y, x+zoom*10, y+zoom*20,  arrow = "last"))
                    if pol_final_tampon[i,j]==3:
                        Pol_line.append(Canevas.create_line(x+zoom*20, y+10*zoom, x, y+zoom*10,  arrow = "last"))
###################################################
Mafenetre = Tk()
Mafenetre.title('MDP')

zoom=1


#taille de la grille
nblignes=15
nbcolonnes=20

#initialisation des paramétre
p=0.6
gamma=0.9
target_recompense=1000
recompense_factor=1

# def des couleurs
myred="#D20B18"
mygreen="#25A531"
myblue="#0B79F7"
mygrey="#E8E8EB"
myyellow="#F9FB70"
myblack="#2D2B2B"
mywalls="#5E5E64"
mywhite="#FFFFFF"
color=[mywhite,mygreen,myblue,myred,myblack]
#les entrées
label_p=Label(Mafenetre, text="p",fg=myblack,font = "Verdana 8 bold")
label_p.pack(side=TOP)
entry_p = DoubleVar()
entry_p.set(p)
e = Entry(Mafenetre, textvariable=entry_p)
e.pack(side=TOP)

label_gamma=Label(Mafenetre, text="gamma",fg=myblack,font = "Verdana 8 bold")
label_gamma.pack(side=TOP)
entry_gamma = DoubleVar()
entry_gamma.set(gamma)
e = Entry(Mafenetre, textvariable=entry_gamma)
e.pack(side=TOP)

label_recompense_factor=Label(Mafenetre, text="recompense_factor",fg=myblack,font = "Verdana 8 bold")
label_recompense_factor.pack(side=TOP)
entry_recompense_factor = DoubleVar()
entry_recompense_factor.set(recompense_factor)
e = Entry(Mafenetre, textvariable=entry_recompense_factor)
e.pack(side=TOP)

label_target_recompense=Label(Mafenetre, text="target_recompense",fg=myblack,font = "Verdana 8 bold")
label_target_recompense.pack(side=TOP)
entry_target_recompense = DoubleVar()
entry_target_recompense.set(target_recompense)
e = Entry(Mafenetre, textvariable=entry_target_recompense)
e.pack(side=TOP)
################################################

# Creation d'un widget Canvas (pour la grille)
Largeur = zoom*20*nbcolonnes+40
Hauteur = zoom*20*nblignes+40
 
# valeurs de la grille
g= np.zeros((nblignes,nbcolonnes), dtype=numpy.int)
cost= np.zeros(5, dtype=numpy.int)
weight= np.zeros(5, dtype=numpy.int)
weight[1] = 1
weight[2] = 2
weight[3] = 3
weight[4] = 4



# ecriture du quadrillage et coloration
Canevas = Canvas(Mafenetre, width = Largeur, height =Hauteur, bg =mywhite)
for i in range(nblignes+1):
    ni=zoom*20*i+20
    Canevas.create_line(20, ni, Largeur-20,ni)
for j in range(nbcolonnes+1):
    nj=zoom*20*j+20
    Canevas.create_line(nj, 20, nj, Hauteur-20)
colordraw(g,nblignes,nbcolonnes)

                
Canevas.focus_set()
Canevas.bind('<Key>',Clavier)
Canevas.pack(padx =5, pady =5)

PosX = 20+10*zoom
PosY = 20+10*zoom

# Creation d'un widget Button (bouton Quitter)
Button(Mafenetre, text ='Restart', command = initialize).pack(side=LEFT,padx=5,pady=5)
Button(Mafenetre, text ='Quit', command = Mafenetre.destroy).pack(side=LEFT,padx=5,pady=5)

Dji_line=[]
Pol_line=[]
test_dji=False
test_val=False
test_pol=False
test_PL=False
pol_final_tampon=[]
#les boutons et leur fonctions associées
Button(Mafenetre, text ='Djikarta', command = djikarta_operation).pack(side=LEFT,padx=5,pady=5)

Button(Mafenetre, text ='Iter Val', command = PDM_Iter_Val).pack(side=LEFT,padx=5,pady=5)

Button(Mafenetre, text ='Iter Pol', command = PDM_Iter_Pol).pack(side=LEFT,padx=5,pady=5)

Button(Mafenetre, text ='PL', command = PDM_PL).pack(side=LEFT,padx=5,pady=5)

w = Label(Mafenetre, text='Cost = '+str(cost[0]),fg=myblack,font = "Verdana 14 bold")
w.pack() 

Pion = Canevas.create_oval(PosX-10,PosY-10,PosX+10,PosY+10,width=2,outline='black',fill=myyellow)


initialize()

#code visant la génération de log pour le rapport
'''
sommetemp1=0
sommetemp2=0
sommetemp=0
sommem1=0
sommem2=0
for po in range (10):
    F = open("MADI_info.txt",'a')
    colordraw(g,nblignes,nbcolonnes)
    temp=PDM_PL()
    sommetemp+=temp
    temp1,m1=PDM_Iter_Val()
    sommem1+=m1
    sommetemp1+=temp1
    temp2,m2=PDM_Iter_Pol()
    sommem2+=m2
    sommetemp2+=temp2
    F.write("essai n°"+str(po)+"  temps PL: "+str(temp)+"   temps iterVal: "+str(temp1)+"   nmb: "+str(m1)+"  temps iterPol: "+str(temp2)+"   nmb: "+str(m2)+'\n')
    F.close()
    g= np.zeros((nblignes,nbcolonnes), dtype=numpy.int)
F = open("MADI_info.txt",'a')
F.write("MOYENNE: "+"  temps PL: "+str(sommetemp/10)+"  temps iterVal: "+str(sommetemp1/10)+"   nmb: "+str(sommem1/10)+"  temps iterPol: "+str(sommetemp2/10)+"   nmb: "+str(sommem2/10)+'\n')
F.close()
'''
Mafenetre.mainloop()
